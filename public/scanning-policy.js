class ScanResultPolicy {
    constructor() {
    }
    toYaml(){
        return "Yaml generation is not implemented yet";
    }
}

class ScanExecutionPolicy {
    constructor() {
      this.type = "scan_execution_policy";
      this.name = "";
      this.description = null;
      this.enabled = false;
      this.projectlabels = [];
      this.rules = [];
      this.actions = [];
    }
    toYaml(){
        let policy = {
          name: this.name
        };
        if(this.description){
            policy.description = this.description;
        }
        policy.enabled = this.enabled;
        if(groupLevel){
            policy.projectlabels=this.projectlabels;
        }
        if(this.rules && this.rules.length>0){
            policy.rules=this.rules;
        }
        if(this.actions && this.actions.length>0){
            policy.actions=JSON.parse(JSON.stringify(this.actions)); //do a deep copy so we can modify stuff
            policy.actions.forEach(function(item){if(item.scan) item.scan=item.scan.toLowerCase();});
        }
        let finalpolicy = {};
        finalpolicy[this.type] = policy;
        return jsyaml.safeDump(finalpolicy, { noArrayIndent: true });
    }
    toRule(){
        let text="";
        let tempactions=[];
        this.actions.forEach((item)=>{const tempaction = actionToText(item); if(tempaction) tempactions.push(tempaction);});
        for(let i=0;i<tempactions.length;i++){
            if(i==0){
                text+=tempactions[i];
            } else {
                if(tempactions.length>2) text+=",";
                if(tempactions.length==i+1) text+=" and";
                text+=" "+tempactions[i];
            }
        }


        let temprules=[];
        //process schedules first
        let schedulerules=[];
        let branches=[];
        let environments=[];
        for(let i=0;i<this.rules.length;i++){
            if(this.rules[i].type){
                if(this.rules[i].type=="schedule"){
                    schedulerules.push(scheduleToText(this.rules[i]));
                } else if(this.rules[i].type=="pipeline"){
                    if(typeof this.rules[i].branches !== 'undefined' && this.rules[i].branches.length>0){
                        branches = branches.concat(this.rules[i].branches);
                    } else if(typeof this.rules[i].environments !== 'undefined' && this.rules[i].environments.length>0){
                        environments = environments.concat(this.rules[i].environments);
                    }
                }
            }
        }
        let scheduleruletext = "";
        for(let i=0;i<schedulerules.length;i++){
            if(i==0){
                scheduleruletext+=schedulerules[i];
            } else {
                if(schedulerules.length>2) scheduleruletext+=",";
                if(schedulerules.length==i+1) scheduleruletext+=" and";
                scheduleruletext+=" "+schedulerules[i];
            }
        }
        if(scheduleruletext.length>0) temprules.push(scheduleruletext);

        //then process pipeline rules
        let pipelinebranches="";
        let pipelineenvironments="";
        for(let i=0;i<branches.length;i++){
            if(i==0){
                pipelinebranches+=branches[i];
            } else {
                if(branches.length>2) pipelinebranches+=",";
                if(branches.length==i+1) pipelinebranches+=" or";
                pipelinebranches+=" "+branches[i];
            }
        }
        for(let i=0;i<environments.length;i++){
            if(i==0){
                pipelineenvironments+=environments[i];
            } else {
                if(environments.length>2) pipelineenvironments+=",";
                if(environments.length==i+1) pipelineenvironments+=" or";
                pipelineenvironments+=" "+environments[i];
            }
        }
        let pipelineruletext=""
        if(pipelinebranches.length>0 || pipelineenvironments.length>0){
            pipelineruletext+="when a pipeline is run against the";
        }
        if(pipelinebranches.length>0){
            pipelineruletext+=" "+pipelinebranches+" branch";
            if(branches.length>1) pipelineruletext+="es";
        }
        if(pipelinebranches.length>0 && pipelineenvironments.length>0){
            pipelineruletext+=" or the";
        }
        if(pipelineenvironments.length>0){
            pipelineruletext+=" "+pipelineenvironments+" environment";
            if(environments.length>1) pipelineruletext+="s";
        }
        if(pipelineruletext.length>0) temprules.push(pipelineruletext);
        
        if(temprules.length==0) return "No rules defined - policy will not run.";
        
        for(let i=0;i<temprules.length;i++){
            if(i==0){
                text+=" "+temprules[i];
            } else {
                if(temprules.length>2) text+=";";
                if(temprules.length==i+1) text+=" or";
                text+=" "+temprules[i];
            }
        }
        if(text=="") return "No rules defined - policy will not run.";
        if(tempactions.length==0) return "No actions defined - policy will not run.";

        if(groupLevel){
           let projectfilters = $("#scanexecutionpolicyprojectlabels").val();
           let labelfiltertext = "";
           for(let i=0;i<projectfilters.length;i++){
               if(i==0){
                labelfiltertext+=" "+projectfilters[i];
               } else {
                   if(projectfilters.length>2) labelfiltertext+=",";
                   if(projectfilters.length==i+1) labelfiltertext+=" or";
                   labelfiltertext+=" "+projectfilters[i];
               }
           }
           text="For projects with the "+labelfiltertext+" label"+(projectfilters.length>=2?"s, ":", ")+text;
        }
        text=text.substring(0,1).toUpperCase()+text.substring(1,text.length)+".";
        return text;
    }
}

class VulnerabilityManagementPolicy {
    constructor() {
    }
    toYaml(){
        return "Yaml generation is not implemented yet";
    }
}

function actionToText(action) {
    let text="";
    if(typeof action.scan !== 'undefined'){
        text+="run a "+action.scan+" scan";
        switch(action.scan){
            case "DAST":
                text+=(action.scanprofile && action.scanprofile.length>0)?" with scan profile "+action.scanprofile:" with no scan profile";
                text+=(action.siteprofile && action.siteprofile.length>0)?" and site profile "+action.siteprofile:" and no site profile";
                break;
        }
    } else if (typeof action.cifile !== 'undefined') {
        if(action.cifile.length==0) return false;
        text+="run the "+action.cifile+" CI file";
    } else if (typeof action.ciblock !== 'undefined') {
        if(action.ciblock.length==0) return false;
        text+="run a custom CI block";
    } else if(typeof action.alert !== 'undefined'){
        text+="send an alert to GitLab";
    } else if(typeof action.slack !== 'undefined'){
        text+=(action.slack.message && action.slack.message.length>0)?"send the "+action.slack.message+" message":"send an empty message";
        text+=(action.slack.channel && action.slack.channel.length>0)?" to the "+action.slack.channel+" channel in Slack":" to no channels in Slack";
    }
    return text;
}

function scheduleToText(schedule){
    let text="on a";
    cadence = schedule.cadence.split(" ");
    text+=(cadence[4]=="*")?" daily basis":" weekly basis on "+numberToDay(cadence[4])+"s";
    text+=cadence[1]<10?" at 0"+cadence[1]+":00":" at "+cadence[1]+":00";
    if(typeof schedule.branches !== 'undefined'){
        text+=" for the "+schedule.branches+" branch";
    } else if(typeof schedule.environments !== 'undefined'){
        text+=" for the "+schedule.environments+" environment";
    }
    return text;
}

function numberToDay(num){
    switch(num){
        case "0":
            return "Sunday";
        case "1":
            return "Monday";
        case "2":
            return "Tuesday";
        case "3":
            return "Wednesday";
        case "4":
            return "Thursday";
        case "5":
            return "Friday";
        case "6":
            return "Saturday";
        default:
            return "";
    }
}